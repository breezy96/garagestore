﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GarageStore.DataAccess.Migrations
{
    public partial class addDeliveryPointToOrderHeader : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DeliveryPoint",
                table: "OrderHeaders",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeliveryPoint",
                table: "OrderHeaders");
        }
    }
}
