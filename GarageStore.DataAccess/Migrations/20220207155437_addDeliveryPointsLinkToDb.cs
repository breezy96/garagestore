﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GarageStore.DataAccess.Migrations
{
    public partial class addDeliveryPointsLinkToDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DeliveryPointsLink",
                table: "DeliveryOptions",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeliveryPointsLink",
                table: "DeliveryOptions");
        }
    }
}
