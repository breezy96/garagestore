﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GarageStore.DataAccess.Migrations
{
    public partial class changeKeyToId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Key",
                table: "OrderHeaders",
                newName: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                table: "OrderHeaders",
                newName: "Key");
        }
    }
}
