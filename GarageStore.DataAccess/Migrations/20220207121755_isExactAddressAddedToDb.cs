﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GarageStore.DataAccess.Migrations
{
    public partial class isExactAddressAddedToDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsExactAddress",
                table: "DeliveryOptions",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsExactAddress",
                table: "DeliveryOptions");
        }
    }
}
