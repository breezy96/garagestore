﻿using GarageStore.DataAccess.Repository.IRepository;
using GarageStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageStore.DataAccess.Repository
{
    public class DeliveryOptionRepository : Repository<DeliveryOption>, IDeliveryOptionRepository
    {
        private ApplicationDbContext _db;

        public DeliveryOptionRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public void Update(DeliveryOption obj)
        {
            _db.DeliveryOptions.Update(obj);
        }
    }
}
