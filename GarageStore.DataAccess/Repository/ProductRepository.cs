﻿using GarageStore.DataAccess.Repository.IRepository;
using GarageStore.Models;

namespace GarageStore.DataAccess.Repository
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        private ApplicationDbContext _db;

        public ProductRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public void Update(Product obj)
        {
            var objFromDb = _db.Products.FirstOrDefault(u => u.Id == obj.Id);
            if (objFromDb != null)
            {
                objFromDb.Title = obj.Title;
                objFromDb.Description = obj.Description;
                objFromDb.ListPrice = obj.ListPrice;
                objFromDb.Price = obj.Price;
                objFromDb.AvailableAmount = obj.AvailableAmount;
                objFromDb.ConditionId = obj.ConditionId;
                objFromDb.Size = obj.Size;
                objFromDb.IsAvailable = obj.IsAvailable;
                objFromDb.CategoryId = obj.CategoryId;
                if (obj.FacebookLink != null)
				{
                    objFromDb.FacebookLink = obj.FacebookLink;
				}
                if (obj.MainImageUrl != null)
                {
                    objFromDb.MainImageUrl = obj.MainImageUrl;
                }
                if (obj.AvailableAmount > 0)
                {
                    objFromDb.IsAvailable = true;
                }
                else
                {
                    objFromDb.IsAvailable = false;
                }
            }
        }
    }
}
