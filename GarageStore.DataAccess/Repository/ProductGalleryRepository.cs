﻿using GarageStore.DataAccess.Repository.IRepository;
using GarageStore.Models;

namespace GarageStore.DataAccess.Repository
{
    public class ProductGalleryRepository : Repository<ProductGallery>, IProductGalleryRepository
    {
        private ApplicationDbContext _db;

        public ProductGalleryRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public void Update(ProductGallery obj)
        {
            _db.ProductGallery.Update(obj);
        }
    }
}
