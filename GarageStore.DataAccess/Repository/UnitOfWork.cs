﻿using GarageStore.DataAccess.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageStore.DataAccess.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private ApplicationDbContext _db;
        public UnitOfWork(ApplicationDbContext db)
        {
            _db = db;
            Category = new CategoryRepository(_db);
            Condition = new ConditionRepository(_db);
            Product = new ProductRepository(_db);
            ProductGallery = new ProductGalleryRepository(_db);
            ShoppingCart = new ShoppingCartRepository(_db);
            ApplicationUser = new ApplicationUserRepository(_db);
            OrderDetail = new OrderDetailRepository(_db);
            OrderHeader = new OrderHeaderRepository(_db);
            DeliveryOption = new DeliveryOptionRepository(_db);
        }
        public ICategoryRepository Category { get; private set; }
        public IConditionRepository Condition { get; private set; }
        public IProductRepository Product { get; private set; }
        public IProductGalleryRepository ProductGallery { get; private set; }
        public IShoppingCartRepository ShoppingCart { get; private set; }
        public IApplicationUserRepository ApplicationUser { get; private set; }
        public IOrderDetailRepository OrderDetail { get; private set; }
        public IOrderHeaderRepository OrderHeader { get; private set; }
        public IDeliveryOptionRepository DeliveryOption { get; private set; }

        public void Save()
        {
            _db.SaveChanges();
        }
    }
}
