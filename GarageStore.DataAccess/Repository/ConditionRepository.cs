﻿using GarageStore.DataAccess.Repository.IRepository;
using GarageStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageStore.DataAccess.Repository
{
    public class ConditionRepository : Repository<Condition>, IConditionRepository
    {
        private ApplicationDbContext _db;

        public ConditionRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public void Update(Condition obj)
        {
            _db.Conditions.Update(obj);
        }
    }
}
