﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageStore.DataAccess.DbInitializer
{
	public interface IDbInitializer
	{
		void Initialize();
	}
}
