﻿using System.ComponentModel.DataAnnotations;

namespace GarageStore.Models
{
    public class Condition
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
