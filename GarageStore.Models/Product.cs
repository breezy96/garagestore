﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GarageStore.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Cím")]
        public string Title { get; set; }
        [Display(Name = "Leírás")]
        public string Description { get; set; }
        [Required]
        [Range(1, 50000)]
        [Display(Name = "Lista ár")]
        public double ListPrice { get; set; }
        [Required]
        [Range(1, 50000)]
        [Display(Name = "Ár")]
        public double Price { get; set; }
        [Required]
        [Range(0, 100)]
        [Display(Name = "Elérhető mennyiség")]
        public int AvailableAmount { get; set; }
        [Required]
        [Display(Name = "Állapot")]
        public int ConditionId { get; set; }
        [ForeignKey("ConditionId")]
        [ValidateNever]
        public Condition Condition { get; set; }
        [Display(Name = "Méret")]
        public string Size { get; set; }
        [Display(Name = "Elérhető")]
        public bool IsAvailable { get; set; } = true;
        [ValidateNever]
        [Display(Name = "Borítókép")]
        public string MainImageUrl { get; set; }
        [NotMapped]
        [ValidateNever]
        [Display(Name = "Galéria képek")]
        public IEnumerable<ProductGallery> ProductGallery { get; set; }
        [Required]
        [Display(Name = "Kategória")]
        public int CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        [ValidateNever]
        public Category Category { get; set; }
        [Display(Name = "Facebook link")]
        public string? FacebookLink { get; set; }
	}
}