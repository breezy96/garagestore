﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageStore.Models
{
    public class OrderHeader
    {
        [Key]
        public int Id { get; set; }
        public string ApplicationUserId { get; set; }
        [ForeignKey("ApplicationUserId")]
        [ValidateNever]
        public ApplicationUser ApplicationUser { get; set; }
        [Required]
        public DateTime OrderDate { get; set; }
        public DateTime ShippingDate { get; set; }
        public double OrderTotal { get; set; }
        public string? OrderStatus { get; set; }
        public string? PaymentStatus { get; set; }
        public string? TrackingNumber { get; set; }
        [Required]
		public int DeliveryOptionId { get; set; }
        [ForeignKey("DeliveryOptionId")]
        [ValidateNever]
		public DeliveryOption DeliveryOption { get; set; }
		public DateTime PaymentDate { get; set; }
        [Required]
        [Display(Name = "Telefonszám")]
        public string PhoneNumber { get; set; }
        [Required]
        [Display(Name = "Utca, házszám")]
        public string StreetAddress { get; set; }
        [Required]
        [Display(Name = "Város")]
        public string City { get; set; }
        [Required]
        [Display(Name = "Irányítószám")]
        public string PostalCode { get; set; }
        [Required]
        [Display(Name = "Név")]
        public string Name { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "E-mail cím")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Kézbesítési pont")]
        public string DeliveryPoint { get; set; }
    }
}
