﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GarageStore.Models
{
    public class ProductGallery
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Display(Name = "URL")]
        public string Url { get; set; }
        [Required]
        [Display(Name = "Termék")]
        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        [ValidateNever]
        public Product Product { get; set; }
    }
}