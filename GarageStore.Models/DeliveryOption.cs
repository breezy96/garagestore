﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageStore.Models
{
	public class DeliveryOption
	{
		[Key]
		public int Id { get; set; }
		[Required]
		public string Carrier { get; set; }
		[Required]
		public string OptionName { get; set; }
		[Required]
		public int Fee { get; set; }
		[Required]
		public bool IsExactAddress { get; set; } = false;
		[Required]
		public bool PayOnDelivery { get; set; } = false;
		public string? DeliveryPointsLink { get; set; }
	}
}
