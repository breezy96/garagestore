﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations;

namespace GarageStore.Models
{
    public class Category
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Név")]
        public string Name { get; set; }
        [Required]
        [Range(1, 10000)]
        [Display(Name = "Megjelenítési sorrend")]
        public int DisplayOrder { get; set; }
        [ValidateNever]
        [Display(Name = "Borítókép")]
        public string ImageUrl { get; set; }
        public DateTime CreatedDateTime { get; set; } = DateTime.Now;
    }
}