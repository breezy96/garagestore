﻿using GarageStore.DataAccess.Repository.IRepository;
using GarageStore.Models;
using GarageStore.Models.ViewModels;
using GarageStore.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace GarageStoreWeb.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = SD.Role_Admin)]
    public class ProductController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _hostEnvironment;

        public ProductController(IUnitOfWork unitOfWork, IWebHostEnvironment hostEnvironment)
        {
            _unitOfWork = unitOfWork;
            _hostEnvironment = hostEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }

        //GET
        public IActionResult Upsert(int? id)
        {
            ProductVM productVM = new()
            {
                Product = new(),
                CategoryList = _unitOfWork.Category.GetAll().Select(u => new SelectListItem
                {
                    Text = u.Name,
                    Value = u.Id.ToString()
                }),
                ConditionList = _unitOfWork.Condition.GetAll().Select(u => new SelectListItem
                {
                    Text = u.Name,
                    Value = u.Id.ToString()
                }),
            };

            if (id == null || id == 0)
            {
                return View(productVM);
            }
            else
            {
                productVM.Product = _unitOfWork.Product.GetFirstOrDefault(u => u.Id == id);
                return View(productVM);
            }
        }

        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Upsert(ProductVM obj, IFormFile? file, IFormFileCollection? files)
        {
            if (ModelState.IsValid)
            {
                string wwwRootPath = _hostEnvironment.WebRootPath;
                if (file != null)
                {
                    string fileName = Guid.NewGuid().ToString();
                    var uploads = Path.Combine(wwwRootPath, @"images\products\main_images");
                    var extension = Path.GetExtension(file.FileName);

                    if (obj.Product.MainImageUrl != null)
                    {
                        var oldImagePath = Path.Combine(wwwRootPath, obj.Product.MainImageUrl.TrimStart('\\'));
                        if (System.IO.File.Exists(oldImagePath))
                        {
                            System.IO.File.Delete(oldImagePath);
                        }
                    }

                    using (var fileStreams = new FileStream(Path.Combine(uploads, fileName + extension), FileMode.Create))
                    {
                        file.CopyTo(fileStreams);
                    }
                    obj.Product.MainImageUrl = @"\images\products\main_images\" + fileName + extension;
                }
                if (obj.Product.Id == 0)
                {
                    _unitOfWork.Product.Add(obj.Product);
                    TempData["success"] = "Termék létrehozása sikeres!";
                }
                else
                {
                    _unitOfWork.Product.Update(obj.Product);
                    TempData["success"] = "Termék frissítése sikeres!";
                }
                _unitOfWork.Save();
                if (files != null && files.Count > 0)
                {
                    IEnumerable<ProductGallery> oldImages = _unitOfWork.ProductGallery.GetAll(filter: x => x.ProductId == obj.Product.Id);
                    if (oldImages != null && oldImages.Count() > 0)
                    {
                        foreach (var imageItem in oldImages)
                        {
                            var imageItemPath = Path.Combine(wwwRootPath, imageItem.Url.TrimStart('\\'));
                            if (System.IO.File.Exists(imageItemPath))
                            {
                                System.IO.File.Delete(imageItemPath);
                            }
                        }
                        _unitOfWork.ProductGallery.RemoveRange(oldImages);
                    }
                    var uploads = Path.Combine(wwwRootPath, @"images\products");
                    foreach (var item in files)
                    {
                        string fileName = Guid.NewGuid().ToString();
                        var extension = Path.GetExtension(item.FileName);
                        
                        using (var fileStreams = new FileStream(Path.Combine(uploads, fileName + extension), FileMode.Create))
                        {
                            item.CopyTo(fileStreams);
                        }
                        ProductGallery productGallery = new()
                        {
                            Url = @"\images\products\" + fileName + extension,
                            ProductId = obj.Product.Id
                        };
                        _unitOfWork.ProductGallery.Add(productGallery);
                    }
                }
                _unitOfWork.Save();
                return RedirectToAction("Index");
            }
            return View(obj);
        }

        #region API CALLS
        [HttpGet]
        public IActionResult GetAll()
        {
            var productList = _unitOfWork.Product.GetAll(includeProperties: "Category,Condition");
            return Json(new { data = productList });
        }

        //POST
        [HttpDelete]
        public IActionResult Delete(int? id)
        {
            var obj = _unitOfWork.Product.GetFirstOrDefault(u => u.Id == id);
            if (obj == null)
            {
                return Json(new { success = false, message = "Hiba törlés közben!" });
            }

            var oldImagePath = Path.Combine(_hostEnvironment.WebRootPath, obj.MainImageUrl.TrimStart('\\'));
            if (System.IO.File.Exists(oldImagePath))
            {
                System.IO.File.Delete(oldImagePath);
            }

            IEnumerable<ProductGallery> oldImages = _unitOfWork.ProductGallery.GetAll(filter: x => x.ProductId == obj.Id);
            if (oldImages != null && oldImages.Count() > 0)
            {
                foreach (var imageItem in oldImages)
                {
                    var imageItemPath = Path.Combine(_hostEnvironment.WebRootPath, imageItem.Url.TrimStart('\\'));
                    if (System.IO.File.Exists(imageItemPath))
                    {
                        System.IO.File.Delete(imageItemPath);
                    }
                }
            }

            _unitOfWork.Product.Remove(obj);
            _unitOfWork.Save();
            return Json(new { success = true, message = "Termék törlése sikeres!" });
        }
        #endregion
    }
}