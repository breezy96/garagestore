﻿using GarageStore.DataAccess.Repository.IRepository;
using GarageStore.Models;
using GarageStore.Models.ViewModels;
using GarageStore.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace GarageStoreWeb.Areas.Admin.Controllers
{
	[Area("Admin")]
	[Authorize]
	public class OrderController : Controller
	{
		private readonly IUnitOfWork _unitOfWork;
		[BindProperty]
		public OrderVM OrderVM { get; set; }
		public OrderController(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}
		public IActionResult Index()
		{
			return View();
		}

		public IActionResult Details(int orderId)
		{
			OrderVM = new OrderVM()
			{
				OrderHeader = _unitOfWork.OrderHeader.GetFirstOrDefault(u => u.Id == orderId,
				includeProperties: "ApplicationUser,DeliveryOption"),
				OrderDetails = _unitOfWork.OrderDetail.GetAll(u => u.OrderId == orderId,
				includeProperties: "Product")
			};
			return View(OrderVM);
		}
		#region API CALLS
		[HttpGet]
		public IActionResult GetAll(string status)
		{
			IEnumerable<OrderHeader> orderHeaders;

			if (User.IsInRole(SD.Role_Admin))
			{
				orderHeaders = _unitOfWork.OrderHeader.GetAll(includeProperties: "ApplicationUser");
			}
			else
			{
				var claimsIdentity = (ClaimsIdentity)User.Identity;
				var claims = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);
				orderHeaders = _unitOfWork.OrderHeader.GetAll(u => u.ApplicationUserId == claims.Value,
					includeProperties: "ApplicationUser");
			}

			switch(status)
			{
				case "pending":
					orderHeaders = orderHeaders.Where(u => u.PaymentStatus == SD.Payment_Status_Pending);
					break;
				case "approved":
					orderHeaders = orderHeaders.Where(u => u.PaymentStatus == SD.Status_Approved);
					break;
				case "shipped":
					orderHeaders = orderHeaders.Where(u => u.PaymentStatus == SD.Status_Shipped);
					break;
				default:
					break;
			}
			orderHeaders = orderHeaders.OrderByDescending(u => u.Id);
			return Json(new { data = orderHeaders });
		}
		#endregion
	}
}
