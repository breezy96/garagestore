﻿using GarageStore.DataAccess.Repository.IRepository;
using GarageStore.Models;
using GarageStore.Models.ViewModels;
using GarageStore.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Security.Claims;

namespace GarageStoreWeb.Areas.Customer.Controllers
{
    [Area("Customer")]
    [Authorize]
    public class CartController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEmailSender _emailSender;
        [BindProperty]
        public ShoppingCartVM ShoppingCartVM { get; set; }
        public CartController(IUnitOfWork unitOfWork, IEmailSender emailSender)
        {
            _unitOfWork = unitOfWork;
            _emailSender = emailSender;
        }
        public IActionResult Index()
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity;
            var claim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);

            ShoppingCartVM = new ShoppingCartVM()
            {
                ListCart = _unitOfWork.ShoppingCart.GetAll(u => u.ApplicationUserId == claim.Value,
                includeProperties: "Product"),
                OrderHeader = new()
            };
            foreach(var cart in ShoppingCartVM.ListCart)
            {
                cart.Price = cart.Product.Price;
                ShoppingCartVM.OrderHeader.OrderTotal += (cart.Price * cart.Count);
            }
            return View(ShoppingCartVM);
        }

        public IActionResult Summary()
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity;
            var claim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);

            ShoppingCartVM = new ShoppingCartVM()
            {
                ListCart = _unitOfWork.ShoppingCart.GetAll(u => u.ApplicationUserId == claim.Value,
                includeProperties: "Product"),
                OrderHeader = new()
            };

            ShoppingCartVM.DeliveryOptions = _unitOfWork.DeliveryOption.GetAll().Select(u => new SelectListItem
            {
                Text = u.OptionName + " ( + " + u.Fee + " Ft )",
                Value = u.Id.ToString()
            });
            ShoppingCartVM.OrderHeader.ApplicationUser = _unitOfWork.ApplicationUser.GetFirstOrDefault(
                u => u.Id == claim.Value);

            ShoppingCartVM.OrderHeader.Name = ShoppingCartVM.OrderHeader.ApplicationUser.Name;
            ShoppingCartVM.OrderHeader.PhoneNumber = ShoppingCartVM.OrderHeader.ApplicationUser.PhoneNumber;
            ShoppingCartVM.OrderHeader.StreetAddress = ShoppingCartVM.OrderHeader.ApplicationUser.StreetAddress;
            ShoppingCartVM.OrderHeader.City = ShoppingCartVM.OrderHeader.ApplicationUser.City;
            ShoppingCartVM.OrderHeader.PostalCode = ShoppingCartVM.OrderHeader.ApplicationUser.PostalCode;
            ShoppingCartVM.OrderHeader.Email = ShoppingCartVM.OrderHeader.ApplicationUser.Email;

            foreach (var cart in ShoppingCartVM.ListCart)
            {
                cart.Price = cart.Product.Price;
                ShoppingCartVM.OrderHeader.OrderTotal += (cart.Price * cart.Count);
            }
            if (ShoppingCartVM.OrderHeader.OrderTotal < SD.Minimum_Order_Value)
                return RedirectToAction("Index", "Home");
            return View(ShoppingCartVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Summary")]
        public IActionResult SummaryPOST()
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity;
            var claim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);

            ShoppingCartVM.ListCart = _unitOfWork.ShoppingCart.GetAll(u => u.ApplicationUserId == claim.Value,
               includeProperties: "Product");
            if (ShoppingCartVM.ListCart == null || ShoppingCartVM.ListCart.Count() == 0)
                return RedirectToAction("Index", "Home");
            ShoppingCartVM.OrderHeader.DeliveryOption = _unitOfWork.DeliveryOption.GetFirstOrDefault(u => u.Id == ShoppingCartVM.OrderHeader.DeliveryOptionId);

            ShoppingCartVM.OrderHeader.PaymentStatus = SD.Payment_Status_Pending;
            ShoppingCartVM.OrderHeader.OrderStatus = SD.Status_Pending;
            ShoppingCartVM.OrderHeader.OrderDate = DateTime.Now;
            ShoppingCartVM.OrderHeader.ApplicationUserId = claim.Value;

            foreach (var cart in ShoppingCartVM.ListCart)
            {
                if (cart.Count > cart.Product.AvailableAmount)
                {
                    HttpContext.Session.Clear();
                    _unitOfWork.ShoppingCart.RemoveRange(ShoppingCartVM.ListCart);
                    _unitOfWork.Save();
                    TempData["error"] = "Sajnos elfogyott a termék, kosara üres lett";
                    return RedirectToAction("Index", "Home");
                }
                cart.Price = cart.Product.Price;
                ShoppingCartVM.OrderHeader.OrderTotal += (cart.Price * cart.Count);
                cart.Product.AvailableAmount -= cart.Count;
                _unitOfWork.Product.Update(cart.Product);
                _unitOfWork.Save();
            }
            if(ShoppingCartVM.OrderHeader.OrderTotal < SD.Delivery_Fee_Zero_Limit)
                ShoppingCartVM.OrderHeader.OrderTotal += ShoppingCartVM.OrderHeader.DeliveryOption.Fee;
            _unitOfWork.OrderHeader.Add(ShoppingCartVM.OrderHeader);
            _unitOfWork.Save();

            foreach (var cart in ShoppingCartVM.ListCart)
            {
                OrderDetail orderDetail = new()
                {
                    ProductId = cart.ProductId,
                    OrderId = ShoppingCartVM.OrderHeader.Id,
                    Price = cart.Price,
                    Count = cart.Count
                };
                _unitOfWork.OrderDetail.Add(orderDetail);
                _unitOfWork.Save();
            }
            HttpContext.Session.Clear();
            _unitOfWork.ShoppingCart.RemoveRange(ShoppingCartVM.ListCart);
            _unitOfWork.Save();

            return RedirectToAction("OrderConfirmation", "Cart", new { id = ShoppingCartVM.OrderHeader.Id });
        }

        public IActionResult OrderConfirmation(int id)
		{
            var orderHeader = _unitOfWork.OrderHeader.GetFirstOrDefault(u => u.Id == id, includeProperties: "ApplicationUser,DeliveryOption");
            var orderDetails = _unitOfWork.OrderDetail.GetAll(u => u.OrderId == id, includeProperties: "Product");
            string emailMessage = "";
            double total = 0;
            foreach(var item in orderDetails)
			{
                total += item.Count * item.Price;
                emailMessage += $"<p>{item.Product.Title} - {item.Price} Ft * {item.Count} db  = {(item.Count * item.Price)} Ft";
			}
            emailMessage += $"<p>+ Szállítás: {((total < SD.Delivery_Fee_Zero_Limit) ? orderHeader.DeliveryOption.Fee : 0)} Ft</p>";
            emailMessage += "<br />";
            emailMessage += $"<p> Összesen: <strong>{orderHeader.OrderTotal} Ft</strong></p>";
            emailMessage += "<br />";

            if (orderHeader.DeliveryOption.Carrier == "Személyes")
			{
                emailMessage += $"<p>Név: {orderHeader.Name}</p>";
                emailMessage += $"<p>Telefonszám: {orderHeader.PhoneNumber}</p>";
                emailMessage += $"<p>E-mail: {orderHeader.Email}</p>";
                emailMessage += $"<p>Cím: {orderHeader.PostalCode}, {orderHeader.City} {orderHeader.StreetAddress}</p>";
                emailMessage += "<br />";
                emailMessage += "<p>Az összeget átvételkor kell fizetni, a részleteket megbeszéljük e-mail-ben vagy telefonon.</p>";
            }
            else if (GetPayOnDeliveryIds().Contains(orderHeader.DeliveryOption.Id))
			{
                emailMessage += $"<p>Név: {orderHeader.Name}</p>";
                emailMessage += $"<p>Telefonszám: {orderHeader.PhoneNumber}</p>";
                emailMessage += $"<p>E-mail: {orderHeader.Email}</p>";
                emailMessage += $"<p>Cím: {orderHeader.PostalCode}, {orderHeader.City} {orderHeader.StreetAddress}</p>";
                emailMessage += $"<p>Választott kézbesítési mód: {orderHeader.DeliveryOption.OptionName}</p>";
                emailMessage += $"<p>Ide: {orderHeader.DeliveryPoint}</p>";
                emailMessage += "<br />";
                emailMessage += "<p>Az összeget átvételkor kell fizetni a kiválasztott kézbesítési ponton.</p>";
            }
			else
			{
                emailMessage += $"<p>Név: {orderHeader.Name}</p>";
                emailMessage += $"<p>Telefonszám: {orderHeader.PhoneNumber}</p>";
                emailMessage += $"<p>E-mail: {orderHeader.Email}</p>";
                emailMessage += $"<p>Cím: {orderHeader.PostalCode}, {orderHeader.City} {orderHeader.StreetAddress}</p>";
                emailMessage += $"<p>Választott kézbesítési mód: {orderHeader.DeliveryOption.OptionName}</p>";
                emailMessage += $"<p>Ide: {orderHeader.DeliveryPoint}</p>";
                emailMessage += "<br />";
                emailMessage += "<p>Az összeget az alábbi bankszámlaszámra legyen szíves utalni, azután fogjuk küldeni a megrendelt termékeket:</p>";
                emailMessage += "<p>Kovács Máté OTP Bank - 11773432-00104685-00000000</p>";
                emailMessage += "<br />";
            }
            emailMessage += "<p>További információért érdeklődjön e-mailben, vagy telefonáljon az alábbi számon:</p>";
            emailMessage += "<br />";
            emailMessage += "<p>+36309293423</p>";
            emailMessage += "<br />";
            emailMessage += "<p>Üdvözlettel:</p>";
            emailMessage += "<br />";
            emailMessage += "<p>Kovács Máté</p>";
            _emailSender.SendEmailAsync(orderHeader.ApplicationUser.Email, $"{orderHeader.Id}. számú rendelés összegzése", emailMessage);
            return View(id);
		}

        public IActionResult Plus(int cartId)
        {
            var cart = _unitOfWork.ShoppingCart.GetFirstOrDefault(u => u.Id == cartId);
            var productFromDb = _unitOfWork.Product.GetFirstOrDefault(u => u.Id == cart.ProductId);
            if (cart.Count >= productFromDb.AvailableAmount)
            {
                TempData["error"] = "Nincs belőle több készleten!";
            }
            else
            {
                _unitOfWork.ShoppingCart.IncrementCount(cart, 1);
                _unitOfWork.Save();
            }
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Minus(int cartId)
        {
            var cart = _unitOfWork.ShoppingCart.GetFirstOrDefault(u => u.Id == cartId);
            if (cart.Count <= 1)
            {
                _unitOfWork.ShoppingCart.Remove(cart);
                var count = _unitOfWork.ShoppingCart.GetAll(u => u.ApplicationUserId == cart.ApplicationUserId).ToList().Count - 1;
                HttpContext.Session.SetInt32(SD.SessionCart, count);
            }
            else
            {
                _unitOfWork.ShoppingCart.DecrementCount(cart, 1);
            }
            _unitOfWork.Save();
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Remove(int cartId)
        {
            var cart = _unitOfWork.ShoppingCart.GetFirstOrDefault(u => u.Id == cartId);
            _unitOfWork.ShoppingCart.Remove(cart);
            _unitOfWork.Save();
            var count = _unitOfWork.ShoppingCart.GetAll(u => u.ApplicationUserId == cart.ApplicationUserId).ToList().Count;
            HttpContext.Session.SetInt32(SD.SessionCart, count);
            return RedirectToAction(nameof(Index));
        }
        public IEnumerable<int> GetPayOnDeliveryIds()
        {
            var id_s = _unitOfWork.DeliveryOption.GetAll(u => u.PayOnDelivery).Select(u => u.Id);
            return id_s;
        }
        #region API CALLS
        [HttpGet]
        public IActionResult GetDeliveryDataForId(int id)
        {
            var data = _unitOfWork.DeliveryOption.GetFirstOrDefault(u => u.Id == id);
            return Json(new { data = data });
        }
		#endregion
	}
}
