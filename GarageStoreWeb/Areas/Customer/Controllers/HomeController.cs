﻿using GarageStore.DataAccess.Repository.IRepository;
using GarageStore.Models;
using GarageStore.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Security.Claims;

namespace GarageStoreWeb.Controllers
{
    [Area("Customer")]
    public class HomeController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, IUnitOfWork unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        public IActionResult Index(string? searchValue = null)
        {
            if (searchValue == null)
            {
                var categoriesFromDb = _unitOfWork.Category.GetAll();
                return View(categoriesFromDb);
            }
            else
            {
                searchValue = searchValue.Replace(" ", String.Empty);
                ViewData["GetSearchDetails"] = searchValue;
                var productsFromDb = _unitOfWork.Product.GetAll(x => x.Title.Contains(searchValue));
                if (productsFromDb == null || productsFromDb.Count() == 0)
                {
                    TempData["error"] = "Nincs a keresésnek megfelelő találat.";
                    return RedirectToAction("Index");
                }
                else return RedirectToAction("Category", new { productIds = productsFromDb.Select(u => u.Id) });
            }
        }

        public IActionResult Category(int categoryId, IEnumerable<int>? productIds = null)
        {
            if (productIds == null || productIds.Count() == 0)
            {
                if (categoryId == 0)
                {
                    return NotFound();
                }
                var productsFromDb = _unitOfWork.Product.GetAll(filter: u => u.CategoryId == categoryId);
                return View(productsFromDb);
            }
            else
            {
                TempData["searchedItems"] = "Keresés eredménye:";
                var productsFromDb = _unitOfWork.Product.GetAll(filter: u => productIds.Contains(u.Id));
                return View(productsFromDb);
            }
        }

        public IActionResult Details(int productId)
        {
            var productFromDb = _unitOfWork.Product.GetFirstOrDefault(u => u.Id == productId, includeProperties: "Category,Condition");
            var productGallery = _unitOfWork.ProductGallery.GetAll(x => x.ProductId == productId);
            productFromDb.ProductGallery = productGallery;
            ShoppingCart shoppingCart = new()
            {
                Count = 1,
                ProductId = productId,
                Product = productFromDb,
            };
            return View(shoppingCart);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public IActionResult Details(ShoppingCart obj)
        {
            var productFromDb = _unitOfWork.Product.GetFirstOrDefault(x => x.Id == obj.ProductId, includeProperties: "Category,Condition");

            var claimsIdentity = (ClaimsIdentity)User.Identity;
            var claim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);
            obj.ApplicationUserId = claim.Value;

            ShoppingCart cartFromDb = _unitOfWork.ShoppingCart.GetFirstOrDefault(
                u => u.ApplicationUserId == claim.Value && u.ProductId == obj.ProductId);

            if (cartFromDb != null)
            {
                if (obj.Count + cartFromDb.Count > productFromDb.AvailableAmount)
                {
                    ModelState.AddModelError("Count", "Nincs ennyi belőle készleten!");
                    var productGallery = _unitOfWork.ProductGallery.GetAll(x => x.ProductId == obj.ProductId);
                    productFromDb.ProductGallery = productGallery;
                    obj.Product = productFromDb;
                    return View(obj);
                }
                _unitOfWork.ShoppingCart.IncrementCount(cartFromDb, obj.Count);
                _unitOfWork.Save();
            }
            else
            {
                if (obj.Count > productFromDb.AvailableAmount)
                {
                    ModelState.AddModelError("Count", "Nincs ennyi belőle készleten!");
                    var productGallery = _unitOfWork.ProductGallery.GetAll(x => x.ProductId == obj.ProductId);
                    productFromDb.ProductGallery = productGallery;
                    obj.Product = productFromDb;
                    return View(obj);
                }
                _unitOfWork.ShoppingCart.Add(obj);
                _unitOfWork.Save();
				HttpContext.Session.SetInt32(SD.SessionCart,
					_unitOfWork.ShoppingCart.GetAll(u => u.ApplicationUserId == claim.Value).ToList().Count);
			}
			TempData["success"] = "Termék sikeresen a kosárba helyezve!";
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}