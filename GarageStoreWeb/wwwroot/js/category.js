﻿var dataTable;

$(document).ready(function () {
    loadDataTable();
});

function loadDataTable() {
    dataTable = $('#tblData').DataTable({
        "ajax": {
            "url": "/Admin/Category/GetAll"
        },
        "columns": [
            {
                "data": "imageUrl",
                "render": function (data) {
                    return `
                        <img src="${data}" class="img-fluid img-thumbnail" />
                        `
                },
                "width": "10%"
            },
            { "data": "name", "width": "25%" },
            {
                "data": "createdDateTime",
                "render": function (data) {
                    var date = new Date(data);
                    var month = date.getMonth() + 1;
                    return date.getFullYear() + "-" + (month.toString().length > 1 ? month : "0" + month) + "-" + date.getDate();
                },
                "width": "20%"
            },
            {
                "data": "id",
                "render": function (data) {
                    return `
                        <div class="w-75 btn-group" role="group">
                        <a href="/Admin/Category/Upsert?id=${data}"
                        class="btn btn-primary mx-2"> <i class="bi bi-pencil-square"></i> Szerkesztés</a>
                        <a onClick=Delete('/Admin/Category/Delete/${data}')
                        class="btn btn-danger mx-2"> <i class="bi bi-trash-fill"></i> Törlés</a>
					</div>
                        `
                },
                "width": "15%"
            }
        ],
        "language": {
            "lengthMenu": "_MENU_ rekord megjelenítése oldalanként",
            "zeroRecords": "Nem található a keresésnek megfelelő adat - sajnáljuk",
            "info": "_PAGE_/_PAGES_ oldal megjelenítése",
            "infoEmpty": "Nem található adat a táblázatban",
            "infoFiltered": "(szűrve _MAX_ rekordból)",
            "search": "Keresés:",
            "paginate": {
                "first": "Első",
                "last": "Utolsó",
                "next": "Következő",
                "previous": "Előző"
            },
        }
    });
}

function Delete(url) {
    Swal.fire({
        title: 'Biztos vagy benne?',
        text: "Ezt nem fogod tudni visszavonni!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Igen, törlés!',
        cancelButtonText: 'Mégse'

    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: url,
                type: 'DELETE',
                success: function (data) {
                    if (data.success) {
                        dataTable.ajax.reload();
                        toastr.success(data.message);
                    }
                    else {
                        toastr.error(data.message);
                    }
                }
            })
        }
    })
}