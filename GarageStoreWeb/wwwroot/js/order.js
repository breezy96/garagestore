﻿var dataTable;

$(document).ready(function () {
    var url = window.location.search;
    if (url.includes("approved")) {
        loadDataTable("approved");
    }
    else {
        if (url.includes("shipped")) {
            loadDataTable("shipped");
        }
        else {
            if (url.includes("pending")) {
                loadDataTable("pending");
            }
            else {
                loadDataTable("all");
            }
        }
    }
});

function loadDataTable(status) {
    dataTable = $('#tblData').DataTable({
        "ajax": {
            "url": "/Admin/Order/GetAll?status=" + status
        },
        "columns": [
            { "data": "id", "width": "5%" },
            { "data": "name", "width": "25%" },
            { "data": "phoneNumber", "width": "15%" },
            { "data": "applicationUser.email", "width": "15%" },
            { "data": "orderStatus", "width": "15%" },
            { "data": "orderTotal", "width": "10%" },
            {
                "data": "id",
                "render": function (data) {
                    return `
                        <div class="w-75 btn-group" role="group">
                        <a href="/Admin/Order/Details?orderId=${data}"
                        class="btn btn-primary mx-2"> <i class="bi bi-pencil-square"></i></a>
					</div>
                        `
                },
                "width": "5%"
            }
        ],
        "language": {
            "lengthMenu": "_MENU_ rekord megjelenítése oldalanként",
            "zeroRecords": "Nem található a keresésnek megfelelő adat - sajnáljuk",
            "info": "_PAGE_/_PAGES_ oldal megjelenítése",
            "infoEmpty": "Nem található adat a táblázatban",
            "infoFiltered": "(szűrve _MAX_ rekordból)",
            "search": "Keresés:",
            "paginate": {
                "first": "Első",
                "last": "Utolsó",
                "next": "Következő",
                "previous": "Előző"
            },
        },
        "order": [[0, 'desc']]
    });
}