﻿var dataTable;

$(document).ready(function () {
    loadDataTable();
});

function loadDataTable() {
    dataTable = $('#tblData').DataTable({
        "ajax": {
            "url": "/Admin/Product/GetAll"
        },
        "columns": [
            {
                "data": "mainImageUrl",
                "render": function (data) {
                    return `
                        <img src="${data}" class="img-fluid img-thumbnail" />
                        `
                },
                "width": "10%"
            },
            { "data": "title", "width": "10%" },
            { "data": "listPrice", "width": "5%" },
            { "data": "availableAmount", "width": "5%" },
            { "data": "condition.name", "width": "5%" },
            { "data": "size", "width": "10%" },
            {
                "data": "isAvailable",
                "render": function (data) {
                    if (data === true) {
                        return `Igen`
                    } else {
                        return `Nem`
                    }
                },
                "width": "5%"

            },
            {
                "data": "id",
                "render": function (data) {
                    return `
                        <div class="w-75 btn-group" role="group">
                        <a href="/Admin/Product/Upsert?id=${data}"
                        class="btn btn-primary mx-2"> <i class="bi bi-pencil-square"></i> Szerkesztés</a>
                        <a onClick=Delete('/Admin/Product/Delete/${data}')
                        class="btn btn-danger mx-2"> <i class="bi bi-trash-fill"></i> Törlés</a>
					</div>
                        `
                },
                "width": "10%"
            }
        ],
        "language": {
            "lengthMenu": "_MENU_ rekord megjelenítése oldalanként",
            "zeroRecords": "Nem található a keresésnek megfelelő adat - sajnáljuk",
            "info": "_PAGE_/_PAGES_ oldal megjelenítése",
            "infoEmpty": "Nem található adat a táblázatban",
            "infoFiltered": "(szűrve _MAX_ rekordból)",
            "search": "Keresés:",
            "paginate": {
                "first": "Első",
                "last": "Utolsó",
                "next": "Következő",
                "previous": "Előző"
            },
        }
    });
}

function Delete(url) {
    Swal.fire({
        title: 'Biztos vagy benne?',
        text: "Ezt nem fogod tudni visszavonni!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Igen, törlés!',
        cancelButtonText: 'Mégse'

    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: url,
                type: 'DELETE',
                success: function (data) {
                    if (data.success) {
                        dataTable.ajax.reload();
                        toastr.success(data.message);
                    }
                    else {
                        toastr.error(data.message);
                    }
                }
            })
        }
    })
}