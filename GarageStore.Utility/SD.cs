﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageStore.Utility
{
    public static class SD
    {
        public const string Role_Admin = "Admin";
        public const string Role_User = "User";

        public const string Status_Pending = "Függőben";
        public const string Status_Approved = "Jóváhagyva";
        public const string Status_Shipped = "Kiszállítva";
        public const string Status_Cancelled = "Visszavonva";

        public const string Payment_Status_Pending = "Függőben";
        public const string Payment_Status_Paid = "Fizetve";

        public const int Delivery_Fee_Zero_Limit = 15000;
        public const int Minimum_Order_Value = 2000;

        public const string SessionCart = "SessionShoppingCart";
    }
}
